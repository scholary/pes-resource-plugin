<?php
/*
Plugin Name: PES Resource Post Type
*/

include_once('lib/CPT.php');

// Plugin expects Advanced Custom Fields plugin to be installed

// create a resource custom post type
$resources = new CPT('resource', array(
    'supports' => array('title', 'editor', 'comments')
));

// TODO add taxonomy e.g. sport, type

// TODO load additional custom fields from ACF (file upload etc)

// use "pages" icon for post type
$resources->menu_icon("dashicons-book-alt");

// Flush rewrite rules
function pes_resource_plugin_flush_rewrite() {
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'pes_resource_plugin_flush_rewrite' );
